import bpy, mathutils

##### CONFIGURATION

## general

HAS_GORGE = True
HAS_SCREW = False
HAS_SLICE = True

## dimensions
# length in millimeters
# rotation in degrees

WALL_THICKNESS = 0.4

# stick #################
STICK_LENGTH = 50
STICK_DIAMETER = 5
# HAS_GORGE >> stick gorge
STICK_GORGE_OFFSET = 5
STICK_GORGE_DIAMETER = STICK_DIAMETER - 1
STICK_GORGE_LENGTH = WALL_THICKNESS * 6.5

# pipe #################
PIPE_DIAMETER = STICK_DIAMETER + WALL_THICKNESS * 6
PIPE_LENGTH = 30
# base
PIPE_BASE_SIZE = [PIPE_DIAMETER*1.5,PIPE_DIAMETER*1.5]
PIPE_BASE_START = 5
PIPE_BASE_POW = 2 # this is the power of the deformation
PIPE_BASE_CUTTER_OFFSET = 1.5
PIPE_BASE_CUTTER_LENGTH = 4.5
# hole
PIPE_HOLE_DIAMETER = STICK_DIAMETER + WALL_THICKNESS
PIPE_HOLE_DEPTH = PIPE_LENGTH - PIPE_BASE_START
# HAS_SCREW >> sidesecrews 
PIPE_SIDESCREW_OFFSET = 20
PIPE_SIDESCREW_LENGTH = WALL_THICKNESS * 10
PIPE_SIDESCREW_WIDTH = PIPE_DIAMETER + WALL_THICKNESS * 4
PIPE_SIDESCREW_DEPTH = WALL_THICKNESS * 10

##### FUNCTIONS

def clear_collection( coll_name ):
    coll2clear = bpy.context.scene.collection.children.get( coll_name )
    for obj in coll2clear.objects:
        bpy.data.objects.remove(obj, do_unlink=True) 

def move_to_collection( obj, coll_name ):
    coll_target = bpy.context.scene.collection.children.get( coll_name )
    for coll in obj.users_collection:
        coll.objects.unlink( obj )
    coll_target.objects.link( obj )

def enable_modifier( mod, enabled ):
    mod.show_in_editmode = 		enabled
    mod.show_render = 			enabled
    mod.show_viewport = 		enabled

def clear_modifier( obj, name, enabled ):
    if not enabled:
        obj.modifiers.remove(obj.modifiers.get(name))

##### OBJECTS

clear_collection( 'stick-pipe' )

tmpl_stick = 				bpy.data.objects['stick']
tmpl_pipe = 				bpy.data.objects['pipe']

stick = 					tmpl_stick.copy()
stick.data = 				tmpl_stick.data.copy()
stick.animation_data_clear()
pipe = 						tmpl_pipe.copy()
pipe.data = 				tmpl_pipe.data.copy()
pipe.animation_data_clear()
move_to_collection( stick, 'stick-pipe' )
move_to_collection( pipe, 'stick-pipe' )

stick_gorge_cutter = 		bpy.data.objects['stick_gorge_cutter']
stick_gorge_inner = 		bpy.data.objects['stick_gorge_inner']
pipe_hole = 				bpy.data.objects['pipe_hole']
pipe_hole_gorge_cutter = 	bpy.data.objects['pipe_hole_gorge_cutter']
pipe_hole_gorge_inner = 	bpy.data.objects['pipe_hole_gorge_inner']
pipe_slicer = 				bpy.data.objects['pipe_slicer']
pipe_base_bottom = 			bpy.data.objects['pipe_base_bottom']
pipe_base_up = 			    bpy.data.objects['pipe_base_up']


##### PROCESS

## configure modifiers
enable_modifier( pipe_hole.modifiers["gorge"],  HAS_GORGE )
clear_modifier( stick,  "gorge",                HAS_GORGE )
clear_modifier( pipe,   "side_screw",           HAS_SCREW )
clear_modifier( pipe,   "bottom_screw",         HAS_SCREW )
clear_modifier( pipe,   "slice",                HAS_SLICE )
clear_modifier( pipe,   "base_up",              not HAS_SLICE )

# stick general
stick.scale = [ STICK_LENGTH, STICK_DIAMETER, STICK_DIAMETER ]
stick_gorge_cutter.scale = [ STICK_GORGE_LENGTH, STICK_DIAMETER*2, STICK_DIAMETER*2 ]
stick_gorge_inner.scale = [ STICK_GORGE_LENGTH*2, STICK_GORGE_DIAMETER, STICK_GORGE_DIAMETER ]
stick.location[0] = (PIPE_LENGTH - PIPE_HOLE_DEPTH) + WALL_THICKNESS
stick_gorge_cutter.location[0] = stick.location[0] + STICK_GORGE_OFFSET
stick_gorge_inner.location[0] = stick.location[0] + STICK_GORGE_OFFSET - STICK_GORGE_LENGTH * 0.5

# pipe geometry
norm_mult = mathutils.Vector((1,(PIPE_BASE_SIZE[0]/PIPE_DIAMETER)-1.0,(PIPE_BASE_SIZE[1]/PIPE_DIAMETER)-1.0))
for i in range(0,len(pipe.data.vertices)):
    co = pipe.data.vertices[i].co
    vec = mathutils.Vector((co[0],co[1],co[2]))
    norm = mathutils.Vector((0,co[1],co[2]))
    
    mult = 1.0 - max(0.0, (vec.x*PIPE_LENGTH)-PIPE_BASE_START) / (PIPE_LENGTH-PIPE_BASE_START)
    
    norm += norm * norm_mult * pow( mult, PIPE_BASE_POW )
    pipe.data.vertices[i].co = [ vec.x, norm.y, norm.z ]

# pipe general
pipe.scale = [ PIPE_LENGTH, PIPE_DIAMETER, PIPE_DIAMETER ]

# pipe hole gorge depends on stick gorge dimension + walls
pipe_hole_gorge_cutter.scale = [stick_gorge_cutter.scale[0]-WALL_THICKNESS*2, PIPE_DIAMETER, PIPE_DIAMETER ]
pipe_hole_gorge_inner.scale = [stick_gorge_inner.scale[0],stick_gorge_inner.scale[1]+WALL_THICKNESS,stick_gorge_inner.scale[2]+WALL_THICKNESS]
pipe_hole.scale = [ PIPE_HOLE_DEPTH + WALL_THICKNESS, PIPE_HOLE_DIAMETER, PIPE_HOLE_DIAMETER ]
pipe_hole.location[0] = PIPE_LENGTH - PIPE_HOLE_DEPTH
pipe_hole_gorge_cutter.location[0] = stick_gorge_cutter.location[0] + WALL_THICKNESS
pipe_hole_gorge_inner.location[0] = stick_gorge_inner.location[0] - WALL_THICKNESS

pipe_slicer.scale = [ PIPE_LENGTH*2, max( PIPE_SIDESCREW_WIDTH*4, PIPE_DIAMETER*4 ), PIPE_DIAMETER*4 ]
pipe_slicer.location[0] = -PIPE_LENGTH*0.5


pipe_base_up.scale = [ PIPE_BASE_CUTTER_LENGTH * 2, PIPE_DIAMETER*2, PIPE_DIAMETER*2 ]
pipe_base_bottom.scale = pipe_base_up.scale
pipe_base_up.location = [ PIPE_BASE_CUTTER_LENGTH, 0, PIPE_BASE_CUTTER_OFFSET ] 
pipe_base_bottom.location = [ PIPE_BASE_CUTTER_LENGTH, 0, -PIPE_BASE_CUTTER_OFFSET ] 