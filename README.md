# TRI LAMP

## shops

tubes (jonc!) carbone et fibre de verre de 1.5 à 8mm de diamètre, maximum 1500mm de long

- https://www.cerfvolantservice.com

## dev

### 2022.02.14

first test

- stick: 5mm fiber glass
- bottom holder: 5mm long
- gut: 2.4mm long on 0.5mm deep

export: stick-holder.20220214-001.stl

not enough room for stick > make the inside larger

export: stick-holder.20220214-002.stl

after test and mounting, stick-holder.20220214-001.stl is good!

parameters:

```
## dimensions
# length in millimeters
# rotation in degrees
WALL_THICKNESS = 0.4
# stick #################
STICK_LENGTH = 50
STICK_DIAMETER = 5
# HAS_GORGE >> stick gorge
STICK_GORGE_OFFSET = 5
STICK_GORGE_DIAMETER = STICK_DIAMETER - 1
STICK_GORGE_LENGTH = WALL_THICKNESS * 6.5
# pipe #################
PIPE_DIAMETER = STICK_DIAMETER + WALL_THICKNESS * 6
PIPE_LENGTH = 30
# base
PIPE_BASE_SIZE = [PIPE_DIAMETER*1.5,PIPE_DIAMETER*1.5]
PIPE_BASE_START = 5
PIPE_BASE_POW = 2 # this is the power of the deformation
PIPE_BASE_CUTTER_OFFSET = 1.5
PIPE_BASE_CUTTER_LENGTH = 4.5
# hole
PIPE_HOLE_DIAMETER = STICK_DIAMETER + WALL_THICKNESS
PIPE_HOLE_DEPTH = PIPE_LENGTH - PIPE_BASE_START
# HAS_SCREW >> sidesecrews 
PIPE_SIDESCREW_OFFSET = 20
PIPE_SIDESCREW_LENGTH = WALL_THICKNESS * 10
PIPE_SIDESCREW_WIDTH = PIPE_DIAMETER + WALL_THICKNESS * 4
PIPE_SIDESCREW_DEPTH = WALL_THICKNESS * 10
```

## wire (in grams)

2022.02.14: 2g + 1g + 2g
